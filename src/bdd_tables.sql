-- Table: label
CREATE TABLE label ( 
    id    INTEGER           PRIMARY KEY,
    label VARCHAR( 0, 40 )  UNIQUE 
);


-- Table: owner
CREATE TABLE owner ( 
    id    INTEGER           PRIMARY KEY,
    name  VARCHAR( 0, 20 )  UNIQUE,
    bank  VARCHAR( 0, 20 ),
    color TEXT,
    account_number TEXT,
    is_active BOOLEAN       DEFAULT (true)
);


-- Table: operation
CREATE TABLE operation ( 
    id          INTEGER        PRIMARY KEY AUTOINCREMENT,
    date        DATETIME,
    amount      REAL,
    id_owner    INTEGER        REFERENCES owner ( id ),
    description TEXT,
    id_label    INTEGER        REFERENCES label ( id ),
    added_date  DATETIME       DEFAULT ( datetime( 0, 'unixepoch' )  ),
    added_hash  VARCHAR( 40 ),
    group_ops   INTEGER,
    UNIQUE ( added_hash ) 
);


-- Table: label_pattern
CREATE TABLE label_pattern ( 
    id          INTEGER         PRIMARY KEY,
    pattern     TEXT            UNIQUE,
    id_label    INTEGER         REFERENCES label ( id ),
    description VARCHAR( 100 ) 
);


CREATE TABLE account_balance ( 
    id       INTEGER  PRIMARY KEY AUTOINCREMENT,
    date     DATETIME,
    id_owner INTEGER  REFERENCES owner ( id ),
    balance  REAL     DEFAULT ( 0 ),
    UNIQUE ( date, id_owner ) 
);



