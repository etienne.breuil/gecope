#include <QApplication>
#include <QTextCodec>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
#endif
	QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("EBreuil");
    QCoreApplication::setApplicationName("GeCoPe");
    QCoreApplication::setApplicationVersion("0.1.0");
	MainWindow w;
	w.show();
	return a.exec();
}
