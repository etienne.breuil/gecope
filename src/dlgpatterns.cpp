#include <QMessageBox>
#include "dlgpatterns.h"
#include "ui_dlgpatterns.h"
#include "tabledata.h"

#define TABLE_LABEL_COL_DISPLAY 1
#define TABLE_PATTERN_COL_DISPLAY 1
#define TABLE_PATTERN_COL_DESC 3

#define INDEX_INTERNALID_LABEL -1
#define isIndexLabel(index) ((int)index.internalId() == INDEX_INTERNALID_LABEL)

PatternModel::PatternModel(QObject *parent, bdd *db) :
    QAbstractItemModel(parent),
    _db(db)
{
    _modelLabel = new QSqlTableModel(this, db->db());
    _modelLabel->setTable("label");
    _modelLabel->setSort(TABLE_LABEL_COL_DISPLAY, Qt::AscendingOrder);
    _modelLabel->setEditStrategy(QSqlTableModel::OnFieldChange);
    _modelLabel->select();

    _modelPattern = new QSqlTableModel(this, db->db());
    _modelPattern->setTable("label_pattern");
    _modelPattern->setEditStrategy(QSqlTableModel::OnFieldChange);

    // Forward rowsRemoved, rowsInserted, dataChanged signals FIXME need to redo select() ?
    connect(_modelLabel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SIGNAL(rowsRemoved(const QModelIndex &, int, int))); // parent is invalid
    connect(_modelLabel, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SIGNAL(rowsInserted(const QModelIndex &, int, int))); // parent is invalid
    connect(_modelLabel, SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)), this, SLOT(fwdModelLabelDataChanged(const QModelIndex &, const QModelIndex &)));
    // FIXME Pattern table events are not forwarded : the current implementation has a filter on one label, thus will not receive all events!
    //connect(_modelPattern, SIGNAL(rowsRemoved(const QModelIndex & parent, int start, int end)), this, SLOT(fwdModelPatternRowsRemoved(const QModelIndex & parent, int start, int end)));
    //connect(_modelPattern, SIGNAL(rowsInserted(const QModelIndex & parent, int start, int end)), this, SLOT(fwdModelPatternRowsInserted(const QModelIndex & parent, int start, int end)));
    //connect(_modelPattern, SIGNAL(dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight)), this, SLOT(fwdModelPatternDataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight)));
    connect(_modelLabel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(revert()));
    connect(_modelLabel, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(revert()));
}

// Forwward Label dataChanged events to the views
void PatternModel::fwdModelLabelDataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight)
{
    if ((topLeft.column() <= TABLE_LABEL_COL_DISPLAY) && ((bottomRight.column() >= TABLE_LABEL_COL_DISPLAY))) {
        emit dataChanged(index(topLeft.row(), 0), index(bottomRight.row(), 0));
    }
}

QModelIndex PatternModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!parent.isValid()) { // root index : list of labels
        return createIndex(row, column, INDEX_INTERNALID_LABEL);
    } else if (isIndexLabel(parent)) { // 1st child : labels
        return createIndex(row, column, parent.row());
    }
    return QModelIndex();
}

QModelIndex PatternModel::parent(const QModelIndex &child) const
{
    if (!isIndexLabel(child)) {
        return createIndex(child.internalId(), 0, INDEX_INTERNALID_LABEL);
    }
    return QModelIndex(); // No parent
}

int PatternModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        return _modelLabel->rowCount();
    } else if (isIndexLabel(parent)) { // 1st child : labels
        select_label(parent.row());
        return _modelPattern->rowCount();
    }
    return 0; // No more children
}

int PatternModel::columnCount(const QModelIndex &parent) const
{
    if (isIndexLabel(parent)) {
        return 1; // 1 for label
    }
    return 3; // 3 for patterns (with nb matching operations and optionnal description)
}

QVariant PatternModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ((role == Qt::DisplayRole) && (orientation == Qt::Horizontal)) {
        switch (section) {
        case 0: return QVariant("");
        case 1: return QVariant(tr("Nb correspondances"));
        case 2: return QVariant(tr("Description"));
        }
    }
    return QVariant();
}

QVariant PatternModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole && role != Qt::EditRole) return QVariant();
    if (!index.isValid()) return QVariant();
    if (index.column() >= columnCount(index)) return QVariant();

    switch (index.column()) {
    case 0:
        if (isIndexLabel(index)) {
            return _modelLabel->data(_modelLabel->index(index.row(), TABLE_LABEL_COL_DISPLAY));
        } else {
            select_label(index.internalId());
            return _modelPattern->data(_modelPattern->index(index.row(), TABLE_PATTERN_COL_DISPLAY));
        }
    case 1:
        if (!isIndexLabel(index)) {
            select_label(index.internalId());
            QString pattern = _modelPattern->data(_modelPattern->index(index.row(), TABLE_PATTERN_COL_DISPLAY)).toString();
            QSqlQuery query;
            query.exec(QString("SELECT count(*) FROM operation WHERE description LIKE '%1'").arg(pattern));
            if (query.next()) {
                return query.value(0);
            }
        }
        break;
    case 2:
        if (!isIndexLabel(index)) {
            select_label(index.internalId());
            return _modelPattern->data(_modelPattern->index(index.row(), TABLE_PATTERN_COL_DESC));
        }
    }
    return QVariant();
}

Qt::ItemFlags PatternModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags ret = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    if (isIndexLabel(index)) {
        if (index.column() == 0) {
            ret |= Qt::ItemIsEditable;
        }
    } else {
        if ((index.column() == 0) || (index.column() == 2)) {
            ret |= Qt::ItemIsEditable;
        }
    }
    return ret;
}

bool PatternModel::setData( const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole) return false;
    if (!index.isValid()) return false;
    if (index.column() >= columnCount(index)) return false;
    int ret = false;
    if (isIndexLabel(index)) {
        ret = _modelLabel->setData(_modelLabel->index(index.row(), TABLE_LABEL_COL_DISPLAY), value);
    } else {
        select_label(index.internalId());
        switch (index.column()) {
        case 0:
            ret = _modelPattern->setData(_modelPattern->index(index.row(), TABLE_PATTERN_COL_DISPLAY), value);
            break;
        case 2:
            ret = _modelPattern->setData(_modelPattern->index(index.row(), TABLE_PATTERN_COL_DESC), value);
        }
    }
    if (ret) {
        emit dataChanged(index, index);
    }
    return ret;
}

bool PatternModel::removeRows(int row, int count, const QModelIndex &parent)
{
    bool ret = false;
    beginRemoveRows(parent, row, row + count);
    if (parent.isValid()) {
        select_label(parent.row());
        qDebug() << "Remove pattern " << data(index(row, 0, parent));
        ret = _modelPattern->removeRows(row, count);
    } else {
        qDebug() << "Remove label " << data(index(row, 0, parent));
        ret = _modelLabel->removeRows(row, count);
    }
    endRemoveRows();
    return ret;
}

void PatternModel::select_label(int row) const
{
    // FIXME a const function that changes the filter!
    static int selected_label = -1;
    if (selected_label != row) {
        selected_label = row;
        int id = _modelLabel->data(_modelLabel->index(row, 0)).toInt();
        _modelPattern->setFilter(QString("id_label = %1").arg(id));
        _modelPattern->select();
        qDebug() << "patternModel : Select label " << row;
    }
}

DlgPatterns::DlgPatterns(bdd *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgPatterns),
    _db(db)
{
    ui->setupUi(this);
    setWindowTitle(tr("Gestion des labels"));
    showMaximized();

    _model = new PatternModel(this, db);
    ui->treeView->setModel(_model);

    QAction *act_suppr = new QAction(tr("Supprimer le motif"), this);
    act_suppr->setShortcut(QKeySequence::Delete);
    connect(act_suppr, SIGNAL(triggered()), this, SLOT(deleteSelection()));
    this->addAction(act_suppr);

    _contextMenu = new QMenu(this);
    _contextMenu->addAction(act_suppr);
    ui->treeView->setColumnWidth(0, 300);
    ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->treeView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customMenuRequested(QPoint)));

    connect(ui->treeView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(treeSelectionChanged(QModelIndex)));
    connect(_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(treeSelectionChanged(QModelIndex)));

    _modelMatches = new QSqlTableModel(this, db->db());
    _modelMatches->setTable("operation");
    _modelMatches->setSort(TableData::OP_COL_DATE, Qt::DescendingOrder); // Sort by date
    _modelMatches->setFilter("1 == 0"); // Inhib
    ui->listFound->setModel(_modelMatches);
    ui->listFound->hideColumn(TableData::OP_COL_ID); // don't show the ID
    _model->setHeaderData(TableData::OP_COL_DATE, Qt::Horizontal, tr("Date"));
    _model->setHeaderData(TableData::OP_COL_AMOUNT, Qt::Horizontal, tr("Montant"));
    ui->listFound->hideColumn(TableData::OP_COL_ACCOUNT); // don't show the Account
    _model->setHeaderData(TableData::OP_COL_DESC, Qt::Horizontal, tr("Opération"));
    ui->listFound->hideColumn(TableData::OP_COL_LABEL); // don't show the type
    ui->listFound->hideColumn(TableData::OP_COL_ADD_DATE);
    ui->listFound->hideColumn(TableData::OP_COL_ADD_HASH);
}

DlgPatterns::~DlgPatterns()
{
    delete ui;
}

void DlgPatterns::customMenuRequested(QPoint pos)
{
    _contextMenu->popup(ui->treeView->viewport()->mapToGlobal(pos));
}

void DlgPatterns::deleteSelection()
{
    foreach (const QModelIndex &index, ui->treeView->selectionModel()->selectedRows()) {
        if (index.parent().isValid() || // No validation for patterns
            (QMessageBox::question(this, "Suppression d'un label",
                                   tr("Voulez vous supprimer le label %1?").arg(index.data().toString()),
                                   QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
           ) {
            _model->removeRow(index.row(), index.parent());
        }
    }
}

void DlgPatterns::treeSelectionChanged(QModelIndex index)
{
    // TODO update listFound model
    if (index.parent().isValid()) {
        QString pattern = _model->data(_model->index(index.row(), 0, index.parent())).toString();
        _modelMatches->setFilter(QString("description LIKE '%1'").arg(pattern));
        _modelMatches->select();
        ui->listFound->resizeColumnsToContents();
        ui->labelMatching->setText(tr("%1 correspondances :").arg(_modelMatches->rowCount()));
    } else {
        _modelMatches->setFilter("1 == 0"); // Inhib
        _modelMatches->select();
        ui->labelMatching->setText("");
    }

}

