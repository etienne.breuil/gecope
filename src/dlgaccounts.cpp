#include "dlgaccounts.h"
#include "ui_dlgaccounts.h"

#include "viewsmodels/colordelegate.h"

DlgAccounts::DlgAccounts(bdd *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgAccounts),
    _db(db)
{
    ui->setupUi(this);
    ui->tableAccounts->setModel(db->model_accounts());
    ui->tableAccounts->hideColumn(bdd::OWNER_COL_ID);



    ColorDelegate *color_delegate = new ColorDelegate(this);
    ui->tableAccounts->setItemDelegateForColumn(bdd::OWNER_COL_COLOR, color_delegate);
}

DlgAccounts::~DlgAccounts()
{
    delete ui;
}

void DlgAccounts::on_btnNewAccount_clicked()
{
    ui->tableAccounts->model()->insertRow(0);
}
