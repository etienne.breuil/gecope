#ifndef DISPRESULTS_H
#define DISPRESULTS_H

#include <QWidget>
#include "bdd.h"

namespace Ui {
    class dispResults;
}

class QwtPlotCurve;
class TimeScaleDraw;

class dispResults : public QWidget
{
    Q_OBJECT

public:
    explicit dispResults(bdd *db, QWidget *parent = 0);
    ~dispResults();

protected slots:
    void updateGraph();

private:
    Ui::dispResults *ui;
    bdd *_db;
    QList<QwtPlotCurve *> _list_curve;
    TimeScaleDraw *_timeScale;
};

#endif // DISPRESULTS_H
