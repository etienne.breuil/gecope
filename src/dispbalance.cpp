#include "dispbalance.h"
#include "ui_dispbalance.h"

#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_grid.h>
#include "plot/timescaledraw.h"
#include "plot/plotdisplayvalue.h"

DispBalance::DispBalance(bdd *db, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DispBalance),
    _db(db)
{
    ui->setupUi(this);
    _db->fill_combo_with_owners(ui->comboAccount);
    ui->textErrors->setReadOnly(true);

    connect(ui->comboAccount, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGraph()));

    QwtLegend *legend = new QwtLegend(this);
    ui->plot->insertLegend(legend);
    ui->plot->setAxisTitle(QwtPlot::yLeft, "€");
    ui->plot->setAxisScaleDraw(QwtPlot::xBottom, new TimeScaleDraw("dd-MM-yyyy"));

    // TODO 1 curve for each month
#if 0
    QwtPlotCurve *curve;
    for (int i = 0; i < _db->model_accounts()->rowCount(); i++) {
        QString account_name = _db->model_accounts()->index(i, bdd::OWNER_COL_NAME).data().toString();
        QString account_color = _db->model_accounts()->index(i, bdd::OWNER_COL_COLOR).data().toString();
        curve = new QwtPlotCurve(account_name);
        curve->setPen(QPen(QColor(account_color)));
        curve->setStyle(QwtPlotCurve::Steps);
        curve->setCurveAttribute(QwtPlotCurve::Inverted);   // Step a droite
        curve->attach(ui->plot);
        _list_curve << curve;
    }

    plotDisplayValue *picker = new plotDisplayValue(curve, (QwtPlotCanvas *)ui->plot->canvas());
    picker->setTrackerMode(QwtPicker::AlwaysOn);

    ui->plot->setAxisScaleDraw(QwtPlot::xBottom, new StockTimeScaleDraw(DATE_FMT));
#else
    QwtPlotCurve *curve = new QwtPlotCurve("");
    //curve->setPen(QPen(QColor(account_color)));
    //curve->setStyle(QwtPlotCurve::Steps);
    //curve->setCurveAttribute(QwtPlotCurve::Inverted);   // Step a droite
    curve->attach(ui->plot);
    _list_curve << curve;
    plotDisplayValue *picker = new plotDisplayValue(curve, (QwtPlotCanvas *)ui->plot->canvas());
    picker->setTrackerMode(QwtPicker::AlwaysOn);
#endif

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(QColor(Qt::gray)));
    grid->attach(ui->plot);

    updateGraph();
}

DispBalance::~DispBalance()
{
    delete ui;
}

void DispBalance::check_balance(QString owner_name, QDate last_date, QDate date, int balance, int computed_balance)
{
    int err = balance - computed_balance;
    QString msg;
    if (err != 0) {
        qDebug() << date.toString("dd/MM/yyyy") << QString(" : balance is wrong : %2€ (waited %1€ : %3€ missing)")
                    .arg(double(balance) / 100).arg(double(computed_balance) / 100).arg(double(err) / 100);
        msg = QString("Erreur de %3€").arg(double(err) / 100);
    } else {
        qDebug() << date.toString("dd/MM/yyyy") << " : balance is correct";
    }
    if (msg.length() > 0)
        ui->textErrors->setPlainText(ui->textErrors->toPlainText() + QString("%1 - %2 (%3) : %4\n").arg(last_date.toString("dd/MM/yyyy")).arg(date.toString("dd/MM/yyyy")).arg(owner_name).arg(msg));
}

int DispBalance::currentAccountId()
{
    return ui->comboAccount->model()->data(ui->comboAccount->model()->index(ui->comboAccount->currentIndex(), bdd::OWNER_COL_ID)).toInt();
}

void DispBalance::updateGraph()
{
    int filter_account_id = currentAccountId();
    QString account_name = _db->model_accounts()->index(ui->comboAccount->currentIndex(), bdd::OWNER_COL_NAME).data().toString();
    QString account_color = _db->model_accounts()->index(ui->comboAccount->currentIndex(), bdd::OWNER_COL_COLOR).data().toString();
    _list_curve.at(0)->setPen(QPen(QColor(account_color)));
    _list_curve.at(0)->setTitle(account_name);

    QSqlQuery q_balance;
    q_balance.exec(QString("SELECT date, balance FROM account_balance "
                            "WHERE id_owner = %1 ORDER BY date ASC").arg(filter_account_id));
    if (!q_balance.next())
        return;		// No balance available
    // Get first values
    QDate first_date = q_balance.value(0).toDate();
    int balance = int(q_balance.value(1).toDouble() * 100);

    ui->textErrors->clear();
    int nb_days = first_date.daysTo(QDate::currentDate()) + 1;
    if (nb_days <= 0)
        return;

    QSqlQuery q_ops;
    q_ops.exec(QString("SELECT date, TOTAL(amount) FROM operation "
                       "WHERE date > '%1' and date <= '%3' AND id_owner = %2 "
                        "GROUP BY date "
                        "ORDER BY date ASC").arg(first_date.toString(Qt::ISODate)).arg(filter_account_id).arg(QDate::currentDate().toString(Qt::ISODate)));

    QVector<double> x(nb_days);
    for (int i = 0; i < nb_days; i++) {
        QDateTime t(first_date.addDays(i));
        x[i] = t.toTime_t();
    }
    QVector<double> y(nb_days, 0.0); // TODO y[nb_curve] : 1 curve for each month

    // Get next checkpoint
    int next_known_day = -1;
    int next_known_balance = 0;
    if (q_balance.next()) {
        next_known_day = first_date.daysTo(q_balance.value(0).toDate());
        next_known_balance = int(q_balance.value(1).toDouble() * 100);
    }

    int prev_day_num = 0;
    QDate last_known_date = first_date;
    while (q_ops.next()) {
        QDate op_date = q_ops.value(0).toDate();
        int day_num = first_date.daysTo(op_date);
        double amount = q_ops.value(1).toDouble();

        for (int i = prev_day_num; i < day_num; i++) {
            y[i] = double(balance) / 100;
        }
        balance += nearbyint(amount * 100);
        y[day_num] = double(balance) / 100;
        prev_day_num = day_num;

        if ((next_known_day >= 0) && (day_num >= next_known_day)) {
            QDate known_date = first_date.addDays(next_known_day);
            //int computed_bal = int(y[next_known_day] * 100);
            check_balance(account_name, last_known_date, known_date, next_known_balance, balance);
            last_known_date = known_date;
#if 0
            // Fix current balance for next values
            balance = next_known_balance;
            if (day_num > next_known_day) {
                balance += amount;
            }
#endif
            // Get next checkpoint
            if (q_balance.next()) {
                next_known_day = first_date.daysTo(q_balance.value(0).toDate());
                next_known_balance = int(q_balance.value(1).toDouble() * 100);
            } else {
                next_known_day = -1; // No more indications
            }
        }

    }
    for (int i = prev_day_num; i < nb_days; i++) {
        y[i] = double(balance) / 100;
    }
    if (next_known_day >= 0) {
        QDate known_date = first_date.addDays(next_known_day);
        //int computed_bal = int(y[next_known_day] * 100);
        check_balance(account_name, last_known_date, known_date, next_known_balance, balance);
        //balance = next_known_balance;
    }

    for (int i = 0; i < _list_curve.size(); i++) { // TODO
#if QWT_VERSION >= 0x060000
        _list_curve.at(i)->setSamples(x, y);
#else
        _list_curve.at(i)->setData(x, y);
#endif
    }

    ui->plot->replot();
    ui->dateBalance->setDate(QDate::currentDate());
    ui->editBalance->setValue(double(balance) / 100);
}


void DispBalance::on_btnAddBalance_clicked()
{
    QString datestr(ui->dateBalance->date().toString(Qt::ISODate));
    double value = ui->editBalance->value();
    QSqlQuery query;
    query.exec(QString("INSERT INTO account_balance (date, id_owner, balance) "
                     "VALUES ('%1', %2, %3)")
             .arg(datestr).arg(currentAccountId()).arg(value));
}
