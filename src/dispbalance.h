#ifndef DISPBALANCE_H
#define DISPBALANCE_H

#include <QWidget>
#include "bdd.h"

#include <qwt_plot_curve.h>

namespace Ui {
class DispBalance;
}

class DispBalance : public QWidget
{
    Q_OBJECT
    
public:
    explicit DispBalance(bdd *db, QWidget *parent = 0);
    ~DispBalance();
    int currentAccountId();

protected slots:
    void updateGraph();

private slots:
    void on_btnAddBalance_clicked();

private:
    void check_balance(QString owner_name, QDate last_date, QDate date, int balance, int computed_balance);

private:
    Ui::DispBalance *ui;
    bdd *_db;
    QList<QwtPlotCurve *> _list_curve;
};

#endif // DISPBALANCE_H
