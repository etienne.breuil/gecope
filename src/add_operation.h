#ifndef ADD_OPERATION_H
#define ADD_OPERATION_H

#include <QDialog>
#include "bdd.h"

namespace Ui {
	class add_operation;
}

class add_operation : public QDialog {
	Q_OBJECT
public:
	add_operation(bdd *db, QWidget *parent = 0);
	~add_operation();

protected:
	void changeEvent(QEvent *e);

private slots:
	void on_buttonBox_accepted();

	void on_btnAddType_clicked();

	void on_editDescription_textChanged(QString );

	void on_btnAddPattern_clicked();

private:
	Ui::add_operation *ui;
	bdd *_db;
};

#endif // ADD_OPERATION_H
