#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QtSql>
#include <QtCore>
#include <QtGui>
#include <QDebug>
#include <QMainWindow>

#include "bdd.h"


namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

    void change_bdd_path(QString filename, bool is_new);
protected:
	void changeEvent(QEvent *e);

private slots:
    void fill_list_owners();
	void importCsv_triggered(QString owner);

    void on_actionNewBdd_triggered();
    void on_actionOpenBdd_triggered();
    void on_actionPatternsLabels_triggered();
    void on_actionAccounts_triggered();

private:
	Ui::MainWindow *ui;
	bdd *_db;
	QMenu *_import_menu;
	QSignalMapper *_importCsv_mapper;
};

#endif // MAINWINDOW_H
