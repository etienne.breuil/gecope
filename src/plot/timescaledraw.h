#ifndef TIMESCALEDRAW_H
#define TIMESCALEDRAW_H

#include <qwt_scale_draw.h>

class TimeScaleDraw: public QwtScaleDraw
{
public:
    TimeScaleDraw(QString fmt) : format(fmt) {}

    void setFormat(QString fmt) { format = fmt; }

    virtual QwtText label(double v) const
    {
    QDateTime t = QDateTime::fromTime_t((int)v);
    return t.toString(format);
    }
private:
    QString format;
};

#endif // TIMESCALEDRAW_H
