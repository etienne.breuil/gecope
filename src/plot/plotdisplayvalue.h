#ifndef PLOTDISPLAYVALUE_H
#define PLOTDISPLAYVALUE_H
#include <qwt_plot_picker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_canvas.h>

class plotDisplayValue : public QwtPlotPicker
{
public:
    plotDisplayValue(QwtPlotCurve *curve, QwtPlotCanvas *canvas);

protected:
    QwtText trackerText(const QPoint &pos) const;
    QwtText trackerTextF(const QPointF &pos) const;

private:
    QwtPlotCurve *m_curve;
};

#endif // PLOTDISPLAYVALUE_H
