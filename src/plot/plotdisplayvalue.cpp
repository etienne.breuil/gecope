#include "plotdisplayvalue.h"
#include <QDebug>
#include <qwt_plot.h>
#include <qwt_scale_draw.h>

plotDisplayValue::plotDisplayValue(QwtPlotCurve *curve, QwtPlotCanvas *canvas) :
	QwtPlotPicker(canvas),
	m_curve(curve)
{
}

static int findXValue(QwtPlotCurve *curve, qreal x)
{
#if QWT_VERSION >= 0x060000
    int ret = curve->data()->size() - 1; // last value
    for (uint i = 1; i < curve->data()->size(); i++) {
        if (x < curve->data()->sample(i).x()) {
#else
    int ret = curve->data().size() - 1; // last value
    for (uint i = 1; i < curve->data().size(); i++) {
        if (x < curve->data().x(i)) {
#endif
            ret = i - 1;
            break;
        }
	}
    return ret;
}

QwtText plotDisplayValue::trackerText(const QPoint &pos) const
{
    return trackerTextF(invTransform(pos));
}

QwtText plotDisplayValue::trackerTextF(const QPointF &pos) const
{
    int idx = findXValue(m_curve, pos.x());
    QwtText date_str = this->plot()->axisScaleDraw(QwtPlot::xBottom)->label(pos.x());
#if QWT_VERSION >= 0x060000
    float amount = m_curve->data()->sample(idx).y();
#else
    float amount = m_curve->data().y(idx);
#endif
    return QwtText(date_str.text() + QString(" : %2 €").arg(amount));
}
