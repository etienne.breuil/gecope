#include "changetypedlg.h"
#include "ui_changetypedlg.h"

changeTypeDlg::changeTypeDlg(bdd *db, QString add_pattern, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::changeTypeDlg),
	_db(db)
{
	ui->setupUi(this);

    _model = new QSqlRelationalTableModel(this, db->db());
    _model->setTable("label");
    _model->setSort (1, Qt::AscendingOrder);
    _model->select();
    ui->comboType->setModel(_model);
    ui->comboType->setModelColumn(1);
	ui->editPattern->setText(add_pattern + "%");
}

changeTypeDlg::~changeTypeDlg()
{
	delete ui;
}

void changeTypeDlg::on_checkAdd_toggled(bool checked)
{
	ui->editPattern->setVisible(checked);
}

void changeTypeDlg::on_buttonBox_accepted()
{
	if (ui->checkAdd->isChecked()) {
		_db->addPattern(ui->comboType->currentText(), ui->editPattern->text());
	}
	_lbl = ui->comboType->currentText();
}

int changeTypeDlg::label_id_selected()
{
    return _model->data(_model->index(ui->comboType->currentIndex(), 0)).toInt();
}

