#include <QFileDialog>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "tabledata.h"
#include "dispresults.h"
#include "disprepartition.h"
#include "dispbalance.h"
#include "dlgpatterns.h"
#include "dlgaccounts.h"

#define SETTING_PATH_BDD "PATH_BDD"
#define SETTING_PATH_IMPORT "PATH_IMPORT_CSV"

#define BDD_FILE_NAME "comptes.sqlite"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	_import_menu(new QMenu("Import csv", this)),
	_importCsv_mapper(new QSignalMapper(this))
{
    ui->setupUi(this);
    showMaximized();

    _db = new bdd(this);
    connect(_importCsv_mapper, SIGNAL(mapped(QString)), this, SLOT(importCsv_triggered(QString)));
    ui->menuImport->addMenu(_import_menu);
    connect(_db, SIGNAL(database_opened(QString)), this, SLOT(fill_list_owners()));
    connect(_db, SIGNAL(database_opened(QString)), statusBar(), SLOT(showMessage(QString)));

    QSettings settings;
    _db->change_path_bdd(settings.value(SETTING_PATH_BDD, QVariant(BDD_FILE_NAME)).toString());

    TableData *tabData = new TableData(_db, this);
    dispResults *dispRes = new dispResults(_db, this);
    dispRepartition *dispRepart = new dispRepartition(_db, this);
    DispBalance *dispBalance = new DispBalance(_db, this);

    ui->tabWidget->removeTab(0);
    ui->tabWidget->addTab(tabData, "Données");
    ui->tabWidget->addTab(dispRes, "Graphiques");
    ui->tabWidget->addTab(dispRepart, "Répartition");
    ui->tabWidget->addTab(dispBalance, "Solde comptes");

    connect(tabData,SIGNAL(display_status(QString)), statusBar(), SLOT(showMessage(QString)));

    if (QCoreApplication::arguments().length() >= 2) {
        QString arg_path = QCoreApplication::arguments().at(1);
        int nb_added = _db->import_file(arg_path);
        QMessageBox::information(this, "Import operations", QString("%1 nouvelles opérations trouvées").arg(nb_added));
    }
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void MainWindow::fill_list_owners()
{
	_import_menu->clear();
	QSqlQuery query;
	_db->exec("SELECT name FROM owner WHERE is_active IS true ORDER BY name", query);
	while (query.next()) {
		QAction *act_owner = new QAction(query.value(0).toString(), this);
		_import_menu->addAction(act_owner);
		connect(act_owner, SIGNAL(triggered()), _importCsv_mapper, SLOT(map()));
		_importCsv_mapper->setMapping(act_owner, query.value(0).toString());
	}
}

void MainWindow::change_bdd_path(QString filename, bool)
{
    QSettings settings;
    settings.setValue(SETTING_PATH_BDD, filename);
    _db->change_path_bdd(filename);
    qDebug() << "FIXME : tout n'est pas mis à jour : relancer l'application avec la nouvelle BDD";
}

void MainWindow::importCsv_triggered(QString owner)
{
    QSettings settings;
    QString dflt_path = settings.value(SETTING_PATH_IMPORT).toString();
    QFileDialog *import_dlg = new QFileDialog(this, "Sélection du fichier à importer", dflt_path, "csv (*.csv);;QIF (*.qif);;All files (*)");
    import_dlg->setFileMode(QFileDialog::ExistingFiles);
    if (import_dlg->exec()) {
        if (dflt_path != import_dlg->directory().absolutePath()) { // Update default directory for import files
            settings.setValue(SETTING_PATH_IMPORT, QVariant(import_dlg->directory().absolutePath()));
        }
        for (int i = 0; i < import_dlg->selectedFiles().count(); i++) {
            QString filename = import_dlg->selectedFiles().at(i);
            int nb_added = _db->import_file(filename, owner);
            QMessageBox::information(this, "Import operations", QString("%1 nouvelles opérations trouvées").arg(nb_added));
        }
    }
    delete import_dlg;
}

void MainWindow::on_actionPatternsLabels_triggered()
{
    DlgPatterns dlg(_db, this);
    dlg.exec();
}

void MainWindow::on_actionAccounts_triggered()
{
    DlgAccounts dlg(_db, this);
    dlg.exec();
}

void MainWindow::on_actionNewBdd_triggered()
{
    QSettings settings;
    QString current_dir = settings.value(SETTING_PATH_BDD, QVariant(BDD_FILE_NAME)).toString();
    QString f_name = QFileDialog::getSaveFileName(this, tr("Base de données des comptes"),
                                                  current_dir, "Sqlite files (*.sqlite);;All files (*)");
    if (!f_name.isEmpty() && f_name != current_dir) {
        change_bdd_path(f_name, true);
    }
}

void MainWindow::on_actionOpenBdd_triggered()
{
    QSettings settings;
    QString current_dir = settings.value(SETTING_PATH_BDD, QVariant(BDD_FILE_NAME)).toString();
    QString f_name = QFileDialog::getOpenFileName(this, tr("Base de données des comptes"),
                                                  current_dir, "Sqlite files (*.sqlite);;All files (*)");
    if (!f_name.isEmpty() && f_name != current_dir) {
        change_bdd_path(f_name, false);
    }
}

