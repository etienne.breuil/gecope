#ifndef BDD_H
#define BDD_H

#include <QObject>
#include <QtSql>
#include <QtCore>
#include <QtGui>
#include <QDebug>

class bdd : public QObject
{
Q_OBJECT
public:
    explicit bdd(QObject *parent);
    void exec(QString sql, QSqlQuery &query);

    bool add_operation(QDate date, float amount, QString owner, QString description, QString type = "", qlonglong accountNum = -1);
    void fill_combo_with_owners(QComboBox *combo);
    void fill_combo_with_labels(QComboBox *combo);

    void regroup_operations(QList<int> list_id);

    int import_file(const QString &file_name, QString owner = QString());
    const QSqlDatabase &db() const {return _db;}

    QSqlTableModel *model_labels() {return _model_labels;}
    QSqlTableModel *model_accounts() {return _model_accounts;}

    enum { // id, name, bank, color, accountNum
        OWNER_COL_ID,
        OWNER_COL_NAME,
        OWNER_COL_BANK,
        OWNER_COL_COLOR,
        OWNER_COL_NUMBER,
        OWNER_COL_IS_ACTIVE,
    };
    enum { // id, label
        LABEL_COL_ID,
        LABEL_COL_NAME
    };

signals:
    void database_opened(QString filename);
    void added_operation(int id);
    void csvFileLoaded(int nb_rows);
public slots:
    void addLabel(QString label);
    void addPattern(QString type, QString pattern);
    void change_path_bdd(QString filename);
private:
    int import_csv_file(const QString &file_name, QString owner);
    int import_qif_file(const QString &file_name, QString owner);

    QSqlDatabase _db;
    QSqlTableModel *_model_labels;
    QSqlTableModel *_model_accounts;
};

#endif // BDD_H
