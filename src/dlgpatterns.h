#ifndef DLGPATTERNS_H
#define DLGPATTERNS_H

#include <QDialog>
#include "bdd.h"

class PatternModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit PatternModel(QObject * parent = 0, bdd *db = 0);
    ~PatternModel() {}

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(const QModelIndex &) const;
    bool setData( const QModelIndex &index, const QVariant &value, int role);
    bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex());

protected:
    void select_label(int row) const;

private slots:
    void fwdModelLabelDataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight);

private:
    bdd *_db;
    QSqlTableModel *_modelLabel;
    QSqlTableModel *_modelPattern;
};


namespace Ui {
class DlgPatterns;
}

class DlgPatterns : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgPatterns(bdd *db, QWidget *parent = 0);
    ~DlgPatterns();

public slots:
    void deleteSelection();
    void treeSelectionChanged(QModelIndex index);

private slots:
    void customMenuRequested(QPoint pos);

private:
    Ui::DlgPatterns *ui;
    bdd *_db;
    PatternModel *_model;
    QMenu *_contextMenu;
    QSqlTableModel *_modelMatches;
};

#endif // DLGPATTERNS_H
