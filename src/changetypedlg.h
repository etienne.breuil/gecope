#ifndef CHANGETYPEDLG_H
#define CHANGETYPEDLG_H

#include <QDialog>
#include "bdd.h"

namespace Ui {
	class changeTypeDlg;
}

class changeTypeDlg : public QDialog
{
	Q_OBJECT

public:
	explicit changeTypeDlg(bdd *db, QString add_pattern, QWidget *parent = 0);
	~changeTypeDlg();
	QString label_selected() { return _lbl; }
    int label_id_selected();

private:
	Ui::changeTypeDlg *ui;
	bdd *_db;
	QString _lbl;
    QSqlTableModel *_model;

private slots:
	void on_buttonBox_accepted();
 void on_checkAdd_toggled(bool checked);
};

#endif // CHANGETYPEDLG_H
