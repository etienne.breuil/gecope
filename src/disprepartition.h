#ifndef DISPREPARTITION_H
#define DISPREPARTITION_H

#include <QWidget>
#include "bdd.h"

namespace Ui {
class dispRepartition;
}

class dispRepartition : public QWidget
{
    Q_OBJECT
    
public:
    explicit dispRepartition(bdd *db, QWidget *parent = 0);
    ~dispRepartition();
    
private slots:
    void on_comboPeriode_currentIndexChanged(int index);

    void on_radio1An_clicked();

    void on_radio1Mois_clicked();

private:
    Ui::dispRepartition *ui;
    bdd *_db;
};

#endif // DISPREPARTITION_H
