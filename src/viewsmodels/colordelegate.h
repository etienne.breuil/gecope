#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include <QItemDelegate>

class ColorDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    explicit ColorDelegate(QObject *parent = 0);

    // painting
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

    // editing
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

signals:
    
public slots:

private slots:
    void editor_accepted();
    void editor_closed();
private:
};

#endif // COLORDELEGATE_H
