#include <QPainter>
#include <QColorDialog>
#include "colordelegate.h"

#define ICON_SIZE 15


ColorDelegate::ColorDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{
}

void ColorDelegate::editor_accepted()
{
    emit commitData((QWidget *)sender());
}
void ColorDelegate::editor_closed()
{
    emit closeEditor((QWidget *)sender());
}

// painting
void ColorDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QColor color = QColor(index.data().toString());
    painter->fillRect(option.rect, color);
}

QSize ColorDelegate::sizeHint(const QStyleOptionViewItem &, const QModelIndex &) const
{
    return QSize(ICON_SIZE, ICON_SIZE);
}

// editing
QWidget *ColorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
    QColorDialog *editor = new QColorDialog(parent);
    connect(editor, SIGNAL(accepted()), this, SLOT(editor_accepted()));
    connect(editor, SIGNAL(rejected()), this, SLOT(editor_closed()));
    editor->open();
    return editor;
}
void ColorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    ((QColorDialog *)editor)->setCurrentColor(QColor(index.data().toString()));
}
void ColorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData(index, QVariant(((QColorDialog *)editor)->currentColor().name()));
}

