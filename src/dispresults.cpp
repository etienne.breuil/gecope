#include "dispresults.h"
#include "ui_dispresults.h"
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include "plot/plotdisplayvalue.h"
#include "plot/timescaledraw.h"

#define DATE_FMT_M "MM-yyyy"
#define DATE_FMT_Y "yyyy"

static void moyenne_glissante(QVector<double> &in, QVector<double> &out)
{
    int factor = 2;
    int nb_in = in.count() - 1; // La dernière valeur correspond à la prochaine période (=0)
    if (nb_in < 2 * factor + 1) { // Pas assez de données pour calculer une tendance
        out << in;
        return;
    }

    float sum = 0.;
    for (int i = 0; i < 2 * factor + 1; i++) // init sum
        sum += in[i];
    for (int i = factor; i < nb_in - factor - 1; i++) {
        out << sum / (2 * factor + 1);
        sum += in[i + factor + 1] - in[i - factor]; // MAJ moyenne pour la prochaine iteration
    }
    sum -= in[nb_in - 1]; // Derniere valeur contient la période courante (incomplete) = 0
    float deriv = out[0] - out[1];
    for (int i = 0; i < factor; i++) { // Fill firsts
        out.prepend(out.first() + deriv);
    }
    deriv = out[out.count() - 2] - out[out.count() - 1];
    for (int i = 0; i < factor + 1; i++) { // Fill lasts
        sum += out.last() - deriv; // Poursuit la moyenne avec une évaluation de la prochaine valeur
        out << sum / (2 * factor + 1);
        sum -= in[nb_in - 2 * factor - 1 + i];
    }
}

dispResults::dispResults(bdd *db, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dispResults),
    _db(db),
    _timeScale(new TimeScaleDraw(DATE_FMT_M))
{
    ui->setupUi(this);
    _db->fill_combo_with_labels(ui->comboType);

    ui->comboXScale->addItem("1 mois", QVariant(1));
    ui->comboXScale->addItem("3 mois", QVariant(3));
    ui->comboXScale->addItem("6 mois", QVariant(6));
    ui->comboXScale->addItem("1 an", QVariant(12));
    ui->comboXScale->setCurrentIndex(0);

    connect(ui->comboType, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGraph()));
    connect(ui->comboXScale, SIGNAL(currentIndexChanged(int)), this, SLOT(updateGraph()));
    connect(ui->cboxCumul, SIGNAL(toggled(bool)), this, SLOT(updateGraph()));

    QwtPlotCurve *curve;
    QwtLegend *legend = new QwtLegend(this);
    for (int i = 0; i < _db->model_accounts()->rowCount(); i++) {
        QString account_name = _db->model_accounts()->index(i, bdd::OWNER_COL_NAME).data().toString();
        QString account_color = _db->model_accounts()->index(i, bdd::OWNER_COL_COLOR).data().toString();
        curve = new QwtPlotCurve(account_name);
        curve->setPen(QPen(QColor(account_color)));
        curve->setStyle(QwtPlotCurve::Steps);
        curve->setCurveAttribute(QwtPlotCurve::Inverted);   // Step a droite
        curve->attach(ui->plot);
        _list_curve << curve;
    }

	curve = new QwtPlotCurve("Total");
	_list_curve << curve;
	curve->setPen(QPen(Qt::black));
	curve->setStyle(QwtPlotCurve::Steps);
    curve->setCurveAttribute(QwtPlotCurve::Inverted);
	curve->attach(ui->plot);
    plotDisplayValue *picker = new plotDisplayValue(curve, (QwtPlotCanvas *)ui->plot->canvas());
    picker->setTrackerMode(QwtPicker::AlwaysOn);

    curve = new QwtPlotCurve("Tendance");
    _list_curve << curve;
    curve->setPen(QPen(Qt::green));
    curve->setStyle(QwtPlotCurve::Lines);
    curve->setCurveAttribute(QwtPlotCurve::Fitted);
    curve->attach(ui->plot);

	QwtPlotGrid *grid = new QwtPlotGrid();
	grid->setPen(QPen(QColor(Qt::gray)));
	grid->attach(ui->plot);

    ui->plot->insertLegend(legend);
    ui->plot->setAxisScaleDraw(QwtPlot::xBottom, _timeScale);
    updateGraph();
}

dispResults::~dispResults()
{
    delete ui;
}

void dispResults::updateGraph()
{
    ui->plot->setAxisTitle(QwtPlot::yLeft, "€");
    int filter_label_id = ui->comboType->model()->data(ui->comboType->model()->index(ui->comboType->currentIndex(), 0)).toInt();
    int nb_month = ui->comboXScale->currentData().toInt();
    bool isCumul = ui->cboxCumul->isChecked();

    _timeScale->setFormat(nb_month < 12 ? DATE_FMT_M : DATE_FMT_Y);

    QSqlQuery query;
    _db->exec("SELECT MIN(date), MAX(date) FROM operation", query);
    if (!query.next())
        return;		// No data?
    QDate oldest_date = query.value(0).toDate();
    QDate newest_date = query.value(1).toDate().addMonths(nb_month); // Permet de dessiner la barre du mois courrant
    QDate datebar(oldest_date.year(), 1 + ((oldest_date.month() - 1) / nb_month) * nb_month, 1); // Démarre sur en début d'année/semestre/trimestre

    QVector<double> x;
    QVector<double> y[_list_curve.size()];
    int moy_idx = _list_curve.size() - 1;
    int tot_idx = moy_idx - 1;

    do {
        QDate date0 = datebar;
        QDate date1 = datebar.addMonths(nb_month).addDays(-1);
        QDateTime tm(datebar);
        x << tm.toTime_t();
        y[tot_idx] << 0.0;

        for (int i = tot_idx - 1; i >= 0; i--) {
            int id_owner = _db->model_accounts()->index(i, bdd::OWNER_COL_ID).data().toInt();
            _db->exec(QString("SELECT -TOTAL(amount) "
                  "FROM operation "
                  "WHERE date BETWEEN '%1' AND '%2' AND id_label = %3 AND id_owner = '%4'")
                  .arg(date0.toString(Qt::ISODate)).arg(date1.toString(Qt::ISODate))
                  .arg(filter_label_id).arg(id_owner), query);
            double sum = 0.0;
            if (query.next())
                sum = query.value(0).toDouble();
            y[tot_idx].last() += sum;
            if (isCumul) {
                y[i] << y[tot_idx].last();
            } else {
                y[i] << sum;
            }
            //qDebug() << datebar.toString("MM-yyyy") << " owner " << id_owner << " : sum= " << sum;
        }

        datebar = datebar.addMonths(nb_month);
    } while(datebar.isValid() && (datebar <= newest_date));

    moyenne_glissante(y[tot_idx], y[moy_idx]);
    for (int i = 0; i < _list_curve.size(); i++) {
        if (isCumul && i < tot_idx) {
            _list_curve.at(i)->setBrush(QBrush(_list_curve.at(i)->pen().color(), Qt::SolidPattern));
        } else {
            _list_curve.at(i)->setBrush(Qt::NoBrush);
        }
#if QWT_VERSION >= 0x060000
        _list_curve.at(i)->setSamples(x, y[i]);
#else
        _list_curve.at(i)->setData(x, y[i]);
#endif
    }

    ui->plot->replot();
}
