#ifndef DLGACCOUNTS_H
#define DLGACCOUNTS_H

#include <QDialog>
#include "bdd.h"

namespace Ui {
class DlgAccounts;
}

class DlgAccounts : public QDialog
{
    Q_OBJECT
    
public:
    explicit DlgAccounts(bdd *db, QWidget *parent = 0);
    ~DlgAccounts();

private slots:
    void on_btnNewAccount_clicked();

private:
    Ui::DlgAccounts *ui;
    bdd *_db;
    QMenu *_contextMenu;
};

#endif // DLGACCOUNTS_H
