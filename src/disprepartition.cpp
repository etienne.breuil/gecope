#include "disprepartition.h"
#include "ui_disprepartition.h"

dispRepartition::dispRepartition(bdd *db, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dispRepartition),
    _db(db)
{
    ui->setupUi(this);
    on_radio1An_clicked();
}

dispRepartition::~dispRepartition()
{
    delete ui;
}

static QTreeWidgetItem *createWidgetItem(QString text, float amount, float total, float last_amount = 0)
{
    float percent = 100. * amount / total;
    float percent_change = 100. * (amount - last_amount) / last_amount;
    QStringList listStr(text);
    listStr << QString("%1€").arg(amount, 0, 'f', 2);
    listStr << QString("%1%").arg(percent, 0, 'f', 2);
    if (last_amount != 0) {
        listStr << QString("%1 (%2%)").arg(amount - last_amount).arg(percent_change, 0, 'f', 2);
    }
    return new QTreeWidgetItem(listStr);
}


void dispRepartition::on_comboPeriode_currentIndexChanged(int)
{
    // TODO change graph
    ui->treePostes->clear();

    QDate date0, date1, date_last;
    if (ui->radio1An->isChecked()) {
        date0.setDate(ui->comboPeriode->currentText().toInt(), 1, 1);
        date1 = date0.addYears(1);
        date_last = date0.addYears(-1);
    } else {
        date0 = QDate::fromString(ui->comboPeriode->currentText(), "MMMM yyyy");
        date1 = date0.addMonths(1);
        date_last = date0.addMonths(-1);
    }
    date1 = date1.addDays(-1); // Dernier jour de la période

    QSqlQuery query;
    _db->exec(QString("SELECT label, -TOTAL(amount) AS total "
          "FROM operation LEFT OUTER JOIN label ON label.id = id_label "
          "WHERE date BETWEEN '%1' AND '%2' GROUP BY label ORDER BY total DESC")
          .arg(date0.toString(Qt::ISODate)).arg(date1.toString(Qt::ISODate)), query);
    float totalPaied = 0.;
    float totalReceived = 0.;
    while(query.next()) {
        float amount = query.value(1).toFloat();
        if (amount > 0) {
            totalPaied += amount;
        } else {
            totalReceived -= amount;
        }
    }

    QTreeWidgetItem *treeIncome = createWidgetItem("Revenus", totalReceived, totalReceived);
    QTreeWidgetItem *treePaied = createWidgetItem("Dépenses et épargnes", totalPaied, totalPaied);
    QFont headFont = treeIncome->font(0);
    headFont.setBold(true);
    for (int i = 0; i < 3; i++) {
        treeIncome->setFont(i, headFont);
        treePaied->setFont(i, headFont);
    }
    ui->treePostes->addTopLevelItem(treeIncome);
    ui->treePostes->addTopLevelItem(treePaied);

    query.first(); // Go back to first entry
    do {
        QSqlQuery query_last;
        _db->exec(QString("SELECT -TOTAL(amount) AS total "
              "FROM operation LEFT OUTER JOIN label ON label.id = id_label "
              "WHERE date BETWEEN '%1' AND '%2' AND label = '%3'")
              .arg(date_last.toString(Qt::ISODate)).arg(date0.addDays(-1).toString(Qt::ISODate)).arg(query.value(0).toString()),query_last);
        float last_amount = (query_last.next() ? query_last.value(0).toFloat() : 0.);
        float amount = query.value(1).toFloat();
        if (amount > 0) {
            treePaied->addChild(createWidgetItem(query.value(0).toString(), amount, totalPaied, last_amount));
        } else {
            treeIncome->addChild(createWidgetItem(query.value(0).toString(), -amount, totalReceived, -last_amount));
        }

        // TODO add icon with color
    } while(query.next());
    ui->treePostes->expandAll();
}

void dispRepartition::on_radio1An_clicked()
{
    // fill comboPeriod
    QSqlQuery query;
    _db->exec("SELECT date FROM operation ORDER BY date ASC LIMIT 1", query);
    if (!query.next())
        return;		// No data?

    ui->comboPeriode->clear();
    for (int year = QDate::currentDate().year(); year >= query.value(0).toDate().year(); year--) {
        ui->comboPeriode->addItem(QString::number(year));
    }

    on_comboPeriode_currentIndexChanged(0);
}

void dispRepartition::on_radio1Mois_clicked()
{
    // fill comboPeriod
    QSqlQuery query;
    _db->exec("SELECT date FROM operation ORDER BY date ASC LIMIT 1", query);
    if (!query.next())
        return;		// No data?

    ui->comboPeriode->clear();
    int firstMonth = QDate::currentDate().month();
    for (int year = QDate::currentDate().year(); year >= query.value(0).toDate().year(); year--) {
        for (int month = firstMonth; month > 0; month--) {
            ui->comboPeriode->addItem(QString("%1 %2").arg(QDate::longMonthName(month)).arg(year));
        }
        firstMonth = 12;
    }

    on_comboPeriode_currentIndexChanged(0);
}
