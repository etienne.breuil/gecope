#include "bdd.h"

static QString computeOperationHash(QDate date, float amount, QString desc)
{
    QCryptographicHash hash(QCryptographicHash::Md4); // No need to get a secure hash
    hash.addData(date.toString(Qt::ISODate).toLatin1());
    hash.addData(QString::number(amount, 'f', 2).toLatin1());
    hash.addData(desc.toLatin1());
    return QString(hash.result().toHex());
}

bdd::bdd(QObject *parent) :
    QObject(parent),
    _db(QSqlDatabase::addDatabase("QSQLITE"))
{
    _model_labels = new QSqlTableModel(this, _db);
    _model_accounts = new QSqlTableModel(this, _db);
}

void bdd::change_path_bdd(QString filename)
{
    if (_db.isOpen()) {
        _db.close();
    }
    _db.setDatabaseName(filename);
    if (!_db.open()) {
        qDebug() << "Failed to open database : " << _db.lastError().text();
        exit(-1); // Should never append
    }
    if (_db.tables().isEmpty()) {
        QFile schemaFile(":/sql/bdd_tables.sql");
        schemaFile.open(QFile::ReadOnly);
        QStringList schemaTableList = QString(schemaFile.readAll()).split(";");
        foreach(const QString schemaTable, schemaTableList) {
            if(!schemaTable.trimmed().isEmpty()) {
                _db.exec(schemaTable);
            }
        }
        schemaFile.close();
        qDebug() << filename << "inited with" << (_db.tables(QSql::Tables).count() - 1) << "tables";
    }
    qDebug() << filename << "opened";

    _model_labels->setTable("label");
    _model_labels->setSort(LABEL_COL_NAME, Qt::AscendingOrder);
    _model_labels->select();
    _model_labels->setEditStrategy(QSqlTableModel::OnFieldChange);
    _model_accounts->setTable("owner");
    _model_accounts->setSort(OWNER_COL_NAME, Qt::AscendingOrder);
    _model_accounts->select();
    _model_accounts->setEditStrategy(QSqlTableModel::OnFieldChange);
    emit database_opened(filename);
}

void bdd::fill_combo_with_owners(QComboBox *combo)
{
    combo->setModel(_model_accounts);
    combo->setModelColumn(OWNER_COL_NAME);
}

void bdd::fill_combo_with_labels(QComboBox *combo)
{
    combo->setModel(_model_labels);
    combo->setModelColumn(LABEL_COL_NAME);
}

bool bdd::add_operation(QDate date, float amount, QString owner, QString description, QString type, qlonglong accountNum)
{
    // the hash is unique, it permits the user to change the date,amount,description after the insertion.
    QString hash = computeOperationHash(date, amount, description);

    QSqlQuery query;
    if(type.isEmpty()) {
        query.prepare("SELECT label, pattern FROM label "
                      "INNER JOIN label_pattern ON label_pattern.id_label = label.id "
                      "WHERE :desc LIKE pattern");
        query.bindValue(":desc", description);
        query.exec();
        if (query.next()) {
            type = query.value(0).toString();
            //qDebug() << "Found label " << type << "for desc = " << description;
        }
    }

    query.prepare("INSERT INTO operation (date, amount, id_owner, id_label, description, added_date, added_hash) "
                  "VALUES (:date, :amount, (SELECT id FROM owner WHERE name = :owner OR account_number = :accountNum), "
                  "(SELECT id FROM label WHERE label = :label), :desc, "
                  "DATETIME('now'), :hash)");
    query.bindValue(":date", date.toString(Qt::ISODate));
    query.bindValue(":amount", amount);
    query.bindValue(":owner", owner);
    query.bindValue(":accountNum", accountNum);
    query.bindValue(":label", type);
    query.bindValue(":desc", description);
    query.bindValue(":hash", hash);
    if (!query.exec()) {
    qDebug() << QString("Insert failed : %1").arg(query.lastError().text());
        return false;
    }
    emit added_operation(query.lastInsertId().toInt());
    emit csvFileLoaded(1); // FIXME
    return true;
}

void bdd::addLabel(QString label)
{
    QSqlQuery query;
    query.exec(QString("INSERT INTO label (label) VALUES ('%1')").arg(label));
    _model_labels->select();
}

void bdd::addPattern(QString label, QString pattern)
{
    QSqlQuery query;
    query.exec(QString("INSERT INTO label_pattern (pattern, id_label) VALUES ('%1', (SELECT id FROM label WHERE label = '%2'))")
               .arg(pattern).arg(label));

    query.exec(QString("UPDATE operation SET id_label = (SELECT id FROM label WHERE label = '%1') WHERE description LIKE '%2'")
               .arg(label).arg(pattern));
}

void bdd::exec(QString sql, QSqlQuery &query)
{
    if (!query.exec(sql)) {
        qDebug() << "Query " << sql << "Failed : " << query.lastError().text();
    }
}

int bdd::import_file(const QString &file_name, QString owner)
{
    if (owner.isNull()) {
        QSqlQuery query;
        query.prepare("SELECT name FROM owner "
                      "WHERE account_number != '' AND :filename LIKE '%'||account_number||'%'");
        query.bindValue(":filename", file_name);
        query.exec();
        if (!query.next()) {
            qDebug() << "No account matching "<< file_name << query.size();
            return -1;
        }
        owner = query.value(0).toString();
    }

    qDebug() << "Import file " << file_name << " for " << owner;

    if (file_name.endsWith(".csv")) {
        return import_csv_file(file_name, owner);
    }
    if (file_name.endsWith(".qif")) {
        return import_qif_file(file_name, owner);
    }

    qDebug() << "No parser matching "<< file_name;
    return -1;
}

int bdd::import_csv_file(const QString &file_name, QString owner)
{
    QFile f(file_name);
    if (!f.open(QIODevice::ReadOnly)) {
        return -1;
    }
    float solde;
    QDate date_solde = QDate();

    qlonglong account_num = 0;
    QSqlQuery query;
    exec(QString("SELECT account_number FROM owner WHERE name = '%1'").arg(owner), query);
    if (query.next())
        account_num = query.value(0).toLongLong();

    int nb_op_found = 0;
    int headers_missing = 4;
    int cell_idx[7];	// date, debit, credit, desc, montant, solde, account_num
    const int CELL_IDX_ING[] = {0, -1, -1, 1, 3, -1, -1};
    memset(cell_idx, -1, sizeof(cell_idx));
    while (!f.atEnd()) {
        QString line = QString::fromLatin1(f.readLine().constData()); // TODO convert ASCII to UTF8

        QStringList list_cell = line.split(";");
        if (headers_missing && (line.split(';')[0].contains(QRegExp("[0-3][0-9]/[0-1][0-9]/")))) {
            memcpy(cell_idx, CELL_IDX_ING, sizeof(cell_idx)); // ING direct data
            headers_missing = 0;
            qDebug() << "No csv header, assume date;desc;;debit/credit;";
        }
        if (headers_missing) {
            if (!line.startsWith("Date", Qt::CaseInsensitive))
                continue; // CE header
            for (int i = 0; i < list_cell.count(); i++) {
                if ((cell_idx[0] == - 1) && list_cell.at(i).startsWith("Date", Qt::CaseInsensitive)) {
                    cell_idx[0] = i;
                    headers_missing--;
                }
                if ((cell_idx[1] == - 1) && list_cell.at(i).contains(QRegExp("D.bit"))) {
                    cell_idx[1] = i;
                    headers_missing--;
                }
                if ((cell_idx[2] == - 1) && list_cell.at(i).contains(QRegExp("Cr.dit"))) {
                    cell_idx[2] = i;
                    headers_missing--;
                }
                if ((cell_idx[3] == - 1) && (list_cell.at(i).startsWith("Libell") || list_cell.at(i).startsWith("label"))) {
                    cell_idx[3] = i;
                    headers_missing--;
                }
                if ((cell_idx[4] == - 1) && (list_cell.at(i).startsWith("Montant") || list_cell.at(i).startsWith("amount"))) {
                    cell_idx[4] = i;
                    headers_missing-=2;
                }
                if ((cell_idx[5] == - 1) && (list_cell.at(i).startsWith("Solde") || list_cell.at(i).startsWith("accountBalance"))) {
                    cell_idx[5] = i;
                }
                if ((cell_idx[6] == - 1) && list_cell.at(i).startsWith("accountNum")) {
                    cell_idx[6] = i;
                }
            }
            if (cell_idx[6] > -1)
                    cell_idx[5] = -1; // FIXME Multicomptes

            qDebug() << QString("Found header : date(%1), Debt(%2), Credit(%3), libelle(%4), Montant(%5), Solde(%6), AccountNum(%7)").
                    arg(cell_idx[0]).arg(cell_idx[1]).arg(cell_idx[2]).arg(cell_idx[3]).arg(cell_idx[4]).arg(cell_idx[5]).arg(cell_idx[6])  ;

            if (headers_missing)
                break;
        } else {
            QString date_str = list_cell.at(cell_idx[0]);
            QDate dateOp = QDate::fromString(date_str,"dd/MM/yyyy");
            if (!dateOp.isValid()) {
                dateOp = QDate::fromString(date_str,"dd/MM/yy");
                if (dateOp.year() < 1970)
                    dateOp = dateOp.addYears(100);
            }
            if (!dateOp.isValid())	// solde debut de periode?
                continue;
            QString amount_str;
            if (cell_idx[4] >= 0) {
                amount_str = list_cell.at(cell_idx[4]);
            } else if (list_cell.at(cell_idx[1]).size() > 0) {
                amount_str = list_cell.at(cell_idx[1]);
            } else {
                amount_str = list_cell.at(cell_idx[2]);
            }
            double amount = amount_str.replace(',','.').remove(QRegExp("[\" ]")).toDouble();
            if (cell_idx[5] >= 0) {
                QString solde_str=list_cell.at(cell_idx[5]);
                solde = solde_str.replace(',','.').remove(QRegExp("[\" ]")).toDouble();
                date_solde = dateOp;
            }
            if (cell_idx[6] >= 0) {
                account_num = QString(list_cell.at(cell_idx[6])).toLongLong();
            }
            QString desc(list_cell.at(cell_idx[3]));
            if(desc.startsWith("\"") && desc.endsWith("\""))
                    desc.remove(QChar('"'));

            qDebug() << "Add operation " << desc << " : " << amount << "€ the " << dateOp.toString("dd/MM/yyyy") << " for account " << account_num;
            if (add_operation(dateOp, amount, "", desc, "", account_num)) {
                nb_op_found++;
            }
        }
    }
    f.close();

    if (date_solde.isValid()) {
        qDebug() << "Solde : " << solde << " the " << date_solde.toString("dd/MM/yyyy");
        _db.exec(QString("INSERT INTO account_balance (date, id_owner, balance) "
                         "VALUES ('%1', (SELECT id FROM owner WHERE name = '%2'), %3)")
                 .arg(date_solde.toString(Qt::ISODate)).arg(owner).arg(solde));
    }

    emit csvFileLoaded(nb_op_found);
    return nb_op_found;
}

int bdd::import_qif_file(const QString &file_name, QString owner)
{
    QFile f(file_name);
    if (!f.open(QIODevice::ReadOnly) || !f.readLine().startsWith('!')) {
        return -1;
    }
    QDate dateOp;
    QString desc;
    QString amount;
    int nb_op_found = 0;

    while (!f.atEnd()) {
        QString line = QString::fromLatin1(f.readLine().constData()); // TODO convert ASCII to UTF8;
        int line_type = line.at(0).unicode();
        QString line_content = line.mid(1).trimmed();
        switch (line_type) {
        case 'D':
            dateOp = QDate::fromString(line_content,"dd/MM/yyyy");
            if (!dateOp.isValid()) {
                dateOp = QDate::fromString(line_content,"dd/MM/yy");
                if (dateOp.year() < 1970)
                    dateOp = dateOp.addYears(100);
            }
            break;
        case 'T':
            amount = line_content;
            break;
        case 'P':
            desc = line_content;
            break;
        case '^':
            qDebug() << "Add operation " << desc << " the " << dateOp.toString("dd/MM/yyyy");
            if (add_operation(dateOp, amount.toDouble(), owner, desc)) {
                nb_op_found++;
            }
            break;
        default:
            break;
        }
    }
    f.close();

    emit csvFileLoaded(nb_op_found);
    return nb_op_found;
}

void bdd::regroup_operations(QList<int> list_id)
{
    QStringList list_id_str;
    foreach(int id, list_id) {
        list_id_str.append(QString::number(id));
    }
    QString join_ids = list_id_str.join(",");
    QSqlQuery query;
    query.exec(QString("SELECT group_ops, count(*) FROM operation "
                       "WHERE group_ops > 0 AND id IN (%1) "
                       "GROUP BY group_ops").arg(join_ids));
    int group_id = list_id.at(0);// First operation id will be the group id
    if (query.next()) { // At least one existing group
        group_id = query.value(0).toInt(); // use the the first group found
        while (query.next()) { // Merge others groups
            _db.exec(QString("UPDATE operation SET group_ops = %1 WHERE group_ops =%2").arg(group_id).arg(query.value(0).toInt()));
        }
    }
    // Set group to selected items
    _db.exec(QString("UPDATE operation SET group_ops = %1 WHERE id IN (%2)").arg(group_id).arg(join_ids));
}

