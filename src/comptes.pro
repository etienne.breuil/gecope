# -------------------------------------------------
# Project created by QtCreator 2011-11-09T21:09:36
# -------------------------------------------------
QT += sql widgets
TARGET = comptes
TEMPLATE = app

win32 {
    include(C:\\Qwt-6.1.3/features/qwt.prf)
}
unix {
    LIBS += -lqwt-qt5
    INCLUDEPATH += /usr/include/qwt
}


SOURCES += main.cpp \
    mainwindow.cpp \
    bdd.cpp \
    add_operation.cpp \
    changetypedlg.cpp \
    dispresults.cpp \
    plot/plotdisplayvalue.cpp \
    disprepartition.cpp \
    tabledata.cpp \
    dlgpatterns.cpp \
    dlgaccounts.cpp \
    viewsmodels/colordelegate.cpp \
    dispbalance.cpp
HEADERS += mainwindow.h \
    bdd.h \
    add_operation.h \
    changetypedlg.h \
    dispresults.h \
    plot/plotdisplayvalue.h \
    disprepartition.h \
    tabledata.h \
    dlgpatterns.h \
    dlgaccounts.h \
    viewsmodels/colordelegate.h \
    dispbalance.h \
    plot/timescaledraw.h
FORMS += mainwindow.ui \
    add_operation.ui \
    changetypedlg.ui \
    dispresults.ui \
    disprepartition.ui \
    tabledata.ui \
    dlgpatterns.ui \
    dlgaccounts.ui \
    dispbalance.ui

RESOURCES += \
    ressources.qrc
