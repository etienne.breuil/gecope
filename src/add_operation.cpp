#include <QDebug>
#include <QInputDialog>

#include "add_operation.h"
#include "ui_add_operation.h"

add_operation::add_operation(bdd *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_operation),
    _db(db)
{
    ui->setupUi(this);
    setWindowTitle(tr("Ajouter une opération"));
    QDoubleValidator *validator = new QDoubleValidator(0, 1000000, 2, this);
    validator->setNotation(QDoubleValidator::StandardNotation);
    validator->setLocale(QLocale::C);
    ui->editAmount->setValidator(validator);

    _db->fill_combo_with_labels(ui->editType);
    _db->fill_combo_with_owners(ui->editAccount);
    ui->editDate->setDate(QDate::currentDate());
}

add_operation::~add_operation()
{
    delete ui;
}

void add_operation::changeEvent(QEvent *e)
{
	QDialog::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void add_operation::on_buttonBox_accepted()
{
    qDebug() << "Add row to db";
    float amount = ui->editAmount->text().toFloat();
    if (!ui->isGain->isChecked())
        amount = -amount;
    if(_db->add_operation(ui->editDate->date(), amount, ui->editAccount->currentText(),
                   ui->editDescription->text().simplified(), ui->editType->currentText())) {
        ui->editAmount->setText("");
        ui->editDescription->setText("");
        ui->lblResult->setText(tr("Donnée ajoutée"));
    } else {
        ui->lblResult->setText(tr("Failed to add data"));
    }


}

void add_operation::on_btnAddType_clicked()
{
	QString newType = QInputDialog::getText(this, "Nom du poste", "Entrez l'intitulé du poste à ajouter");
	if (!newType.isEmpty()) {
		_db->addLabel(newType);
        ui->editType->setCurrentIndex(ui->editType->findText(newType));
	}
}

void add_operation::on_editDescription_textChanged(QString newDesc)
{
    QSqlQuery query;
    query.prepare("SELECT label, pattern FROM label "
                  "INNER JOIN label_pattern ON label_pattern.id_label = label.id "
                  "WHERE :desc LIKE pattern");
    query.bindValue(":desc", newDesc.simplified());
    query.exec();
    if (query.next()) {
        QString type = query.value(0).toString();
        ui->editType->setCurrentIndex(ui->editType->findText(type));
    }
}

void add_operation::on_btnAddPattern_clicked()
{
    QString pattern = QInputDialog::getText(this, ui->editType->currentText(), "Entrez le pattern a ajouter", QLineEdit::Normal, ui->editDescription->text().simplified() + "%");
    if (!pattern.isEmpty()) {
        _db->addPattern(ui->editType->currentText(), pattern);
    }
}
