#include <QMessageBox>

#include "tabledata.h"
#include "ui_tabledata.h"

#include "changetypedlg.h"
#include "add_operation.h"

// TODO : Améliorer ajout des nouvelles lignes par formulaire (formulaire en view/model)
// TODO : Ajouter des points de synchro (solde à une date donnée), vérification d'erreurs, graphique d'évolution du compte jour/jour, moyenne sur le mois...
// TODO : Ajouter la possibilité de lier des dépenses à des remboursements

// TODO : Mettre tous les models SQL dans la classe Bdd pour une update simplifiée

void BackgroundColorDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);
    if (index.column() == TableData::OP_COL_AMOUNT) {
        QColor c = index.model()->data(index).toDouble() > 0 ? Qt::green : Qt::red;
        option->backgroundBrush = QBrush(c.lighter(160));
    }
    if (index.column() == TableData::OP_COL_DATE) {
        QDate date_add = index.sibling(index.row(),TableData::OP_COL_ADD_DATE).data().toDate();
        if (date_add == QDate::currentDate()) {
            option->font.setBold(true);
        }
    }
}

#if 0
// FIXME Casse les relationaldelegates!
//ReadOnlyColumnsProxyModel *proxymodel = new ReadOnlyColumnsProxyModel(this);
//proxymodel->setSourceModel(_model);
//ui->dataView->setModel(proxymodel);

Qt::ItemFlags ReadOnlyColumnsProxyModel::flags( const QModelIndex & index ) const
{
    Qt::ItemFlags ret = QIdentityProxyModel::flags(index);
    if ((index.column() == TableData::OP_COL_ID)
            || (index.column() == TableData::OP_COL_ADD_DATE)
            || (index.column() == TableData::OP_COL_ADD_HASH)) {
        return ret &= ~Qt::ItemIsEditable; // Disable edit
    }
    return ret;
}
#endif

TableData::TableData(bdd *db, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TableData),
    _db(db)
{
    ui->setupUi(this);

    _model = new QSqlRelationalTableModel(this, db->db());
    _model->setTable("operation");
    _model->setEditStrategy(QSqlTableModel::OnFieldChange);

    _model->setSort (OP_COL_DATE, Qt::DescendingOrder);
    _model->setRelation(OP_COL_ACCOUNT, QSqlRelation("owner", "id", "name"));
    _model->setRelation(OP_COL_LABEL, QSqlRelation("label", "id", "label"));
    _model->setJoinMode(QSqlRelationalTableModel::LeftJoin);
    _model->relationModel(OP_COL_ACCOUNT)->setSort(bdd::OWNER_COL_NAME, Qt::AscendingOrder);
    _model->relationModel(OP_COL_LABEL)->setSort(bdd::LABEL_COL_NAME, Qt::AscendingOrder);
    _model->relationModel(OP_COL_ACCOUNT)->select();
    _model->relationModel(OP_COL_LABEL)->select();

    _model->select();
    _model->setHeaderData(OP_COL_ACCOUNT, Qt::Horizontal, tr("Compte"));
    _model->setHeaderData(OP_COL_DATE, Qt::Horizontal, tr("Date"));
    _model->setHeaderData(OP_COL_DESC, Qt::Horizontal, tr("Opération"));
    _model->setHeaderData(OP_COL_AMOUNT, Qt::Horizontal, tr("Montant"));
    _model->setHeaderData(OP_COL_LABEL, Qt::Horizontal, tr("Type"));

    ui->dataView->setModel(_model);
    ui->dataView->hideColumn(OP_COL_ID); // don't show the ID
    ui->dataView->hideColumn(OP_COL_ADD_HASH);
    //ui->dataView->hideColumn(OP_COL_GROUP);
    ui->dataView->setItemDelegate(new QSqlRelationalDelegate(ui->dataView)); // Combobox for edition
    ui->dataView->horizontalHeader()->moveSection(OP_COL_LABEL, OP_COL_LABEL - 1); // Invert OP_COL_LABEL and OP_COL_DESC

    ui->dataView->resizeColumnsToContents();

    _db->fill_combo_with_labels(ui->ComboFilterType);
    _db->fill_combo_with_owners(ui->ComboFilterAccount);
    ui->ComboFilterType->setCurrentIndex(-1);
    ui->ComboFilterAccount->setCurrentIndex(-1);
    connect(ui->editFilter, SIGNAL(editingFinished()), this, SLOT(updateFilter()));
    connect(ui->ComboFilterType, SIGNAL(currentIndexChanged(int)), this, SLOT(updateFilter()));
    connect(ui->ComboFilterAccount, SIGNAL(currentIndexChanged(int)), this, SLOT(updateFilter()));

    BackgroundColorDelegate *bg_delegate = new BackgroundColorDelegate();
    ui->dataView->setItemDelegateForColumn(OP_COL_AMOUNT, bg_delegate);
    ui->dataView->setItemDelegateForColumn(OP_COL_DATE, bg_delegate);

    QItemSelectionModel *sm = ui->dataView->selectionModel();
    connect(sm, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(selectionChanged(QItemSelection,QItemSelection)));

    ui->dataView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->dataView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customMenuRequested(QPoint)));

    QAction *act_suppr = new QAction(tr("Supprimer les données"), this);
    act_suppr->setShortcut(QKeySequence::Delete);
    connect(act_suppr, SIGNAL(triggered()), this, SLOT(deleteSelection()));

    QAction *act_chg_label = new QAction(tr("Changer le label"), this);
    connect(act_chg_label, SIGNAL(triggered()), this, SLOT(changeSelectionLabel()));

    QAction *act_group = new QAction(tr("Lier les opérations"), this);
    connect(act_group, SIGNAL(triggered()), this, SLOT(changeSelectionGroup()));

    _contextMenu = new QMenu(this);
    _contextMenu->addAction(act_suppr);
    _contextMenu->addAction(act_chg_label);
    _contextMenu->addAction(act_group);
    this->addAction(act_suppr);
    this->addAction(act_chg_label);
    this->addAction(act_group);

    connect(_db, SIGNAL(csvFileLoaded(int)), this, SLOT(tableUpdated()));
    connect(_db, SIGNAL(database_opened(QString)), this, SLOT(tableUpdated()));
}

TableData::~TableData()
{
    delete ui;
}

void TableData::tableUpdated()
{
    _model->select(); // Update the model
}

void TableData::selectionChanged(QItemSelection,QItemSelection)
{
    float total_debt = 0.;
    float total_cred = 0.;
    int nb_elem = 0;
    foreach(QModelIndex index, ui->dataView->selectionModel()->selectedRows(OP_COL_AMOUNT)) {
        double v =  _model->data(index).toDouble();
        if (v > 0) {
            total_cred += v;
        } else {
            total_debt -= v;
        }
        nb_elem++;
    }
    QString msg("");
    if (nb_elem > 0) {
        msg.append(tr("Sur %1 transactions : ").arg(nb_elem));
        if (total_debt > 0.)
            msg.append(tr("Débit : %1€ ").arg(total_debt));
        if (total_cred > 0.)
            msg.append(tr("Crédit : %1€ ").arg(total_cred));
        if ((total_cred > 0.) && (total_debt > 0.))
            msg.append(tr("Total : %1€ ").arg(total_cred - total_debt));
    }
    emit display_status(msg);
}

void TableData::updateFilter()
{
    QStringList list_filters;
    if (!ui->editFilter->text().isEmpty()) {
        list_filters.append(QString("description LIKE '\%%1\%'").arg(ui->editFilter->text()));
    }
    if (ui->ComboFilterType->currentIndex() != -1) {
        int id_label = ui->ComboFilterType->model()->index(ui->ComboFilterType->currentIndex(), 0).data().toInt();
        list_filters.append(QString("id_label = %1").arg(id_label));
    }
    if (ui->ComboFilterAccount->currentIndex() != -1) {
        int id_owner = ui->ComboFilterAccount->model()->index(ui->ComboFilterAccount->currentIndex(), 0).data().toInt();
        list_filters.append(QString("id_owner = %1").arg(id_owner));
    }
    _model->setFilter(list_filters.join(" AND "));
}

void TableData::on_btnResetFilter_clicked()
{
    ui->ComboFilterType->setCurrentIndex(-1);
    ui->ComboFilterAccount->setCurrentIndex(-1);
    ui->editFilter->setText("");
    updateFilter();
}

void TableData::customMenuRequested(QPoint pos)
{
    _contextMenu->popup(ui->dataView->viewport()->mapToGlobal(pos));
}


void TableData::deleteSelection()
{
    if (QMessageBox::question(this, "Suppression des enregistrements",
                  QString("Voulez vous supprimer les enregistrements sélectionnés?"),
                  QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
        // List the rows, order them, then remove them from the bottom (to avoid a shift)
        QList<int> rows;
        foreach( const QModelIndex &index, ui->dataView->selectionModel()->selectedRows()) {
           rows.append(index.row());
        }
        qSort( rows );
        for (int i = rows.count() - 1; i >= 0; i -= 1) {
            _model->removeRow(rows[i]); // FIXME slow!!
        }
    }
}

void TableData::changeSelectionLabel()
{
    if (ui->dataView->selectionModel()->selectedRows().length() == 0) {
        return;
    }
    QString dflt_pattern = ui->dataView->selectionModel()->selectedRows(OP_COL_DESC).first().data().toString();
    _model->setEditStrategy(QSqlTableModel::OnManualSubmit);

    changeTypeDlg dlg(_db, dflt_pattern, this);
    if (dlg.exec() == QDialog::Accepted) {
        int label_id = dlg.label_id_selected();
        foreach (const QModelIndex &index, ui->dataView->selectionModel()->selectedRows(OP_COL_LABEL)) {
            _model->setData(index, label_id);
        }
    }

    _model->submitAll();
    _model->setEditStrategy(QSqlTableModel::OnFieldChange);
}

void TableData::changeSelectionGroup()
{
    if (ui->dataView->selectionModel()->selectedRows().length() < 2) {
        return;
    }
    QList<int> list_op_id;
    foreach (const QModelIndex &index, ui->dataView->selectionModel()->selectedRows(OP_COL_ID)) {
        list_op_id.append(index.data().toInt());
    }
    _db->regroup_operations(list_op_id);
    _model->select();
}

void TableData::on_btnAddData_clicked()
{
    add_operation add_dialog(_db, this);
    add_dialog.exec();
}

