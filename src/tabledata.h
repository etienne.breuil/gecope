#ifndef TABLEDATA_H
#define TABLEDATA_H

#include <QWidget>
#include <QStyledItemDelegate>
#include <QMenu>

#include "bdd.h"

namespace Ui {
class TableData;
}

#if 0
class ReadOnlyColumnsProxyModel : public QIdentityProxyModel
{
    Q_OBJECT
public:
    explicit ReadOnlyColumnsProxyModel(QObject *parent = 0) : QIdentityProxyModel(parent) {}

    Qt::ItemFlags flags ( const QModelIndex & index ) const;
};
#endif

class BackgroundColorDelegate : public QStyledItemDelegate {
public:
    BackgroundColorDelegate(QObject *parent = 0): QStyledItemDelegate(parent) {}
    void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const;
};

class TableData : public QWidget
{
    Q_OBJECT
    
public:
    explicit TableData(bdd *db, QWidget *parent = 0);
    ~TableData();

    enum { // id, date, amount, id_owner, description, id_label
        OP_COL_ID,
        OP_COL_DATE,
        OP_COL_AMOUNT,
        OP_COL_ACCOUNT,
        OP_COL_DESC,
        OP_COL_LABEL,
        OP_COL_ADD_DATE,
        OP_COL_ADD_HASH,
        OP_COL_GROUP
    };

public slots:
    void tableUpdated();

private slots:
    void selectionChanged(QItemSelection selected,QItemSelection deselected);
    void updateFilter();
    void on_btnResetFilter_clicked();

    void customMenuRequested(QPoint pos);
    void deleteSelection();
    void changeSelectionLabel();
    void changeSelectionGroup();
    void on_btnAddData_clicked();

signals:
    void display_status(QString msg);

private:
    Ui::TableData *ui;
    bdd *_db;
    QSqlRelationalTableModel *_model;
    QMenu *_contextMenu;
};

#endif // TABLEDATA_H
